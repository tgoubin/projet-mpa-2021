██ ██    ██ ████████     ██████  ██       █████   ██████  ███    ██  █████   ██████ 
██ ██    ██    ██        ██   ██ ██      ██   ██ ██       ████   ██ ██   ██ ██      
██ ██    ██    ██        ██████  ██      ███████ ██   ███ ██ ██  ██ ███████ ██      
██ ██    ██    ██        ██   ██ ██      ██   ██ ██    ██ ██  ██ ██ ██   ██ ██      
██  ██████     ██        ██████  ███████ ██   ██  ██████  ██   ████ ██   ██  ██████ 
                                                                                    
                                                                                    
                                                                                                                
 ▄▄       ▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ 
▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌░▌   ▐░▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌
▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌▐░▌       ▐░▌
▐░▌ ▐░▐░▌ ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌
▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌
▐░▌   ▀   ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌
▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌
▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌
 ▀         ▀  ▀            ▀         ▀ 
 
 How to generate the documentation and build the application :
 > mvn clean package
   
 Documentation path: project-mpa-2021-documentation/target/readme.html

 Application path: project-mpa-2021-delivery/target/quizz.zip
                                   
 How to launch the application:
 
 - unzip "project-mpa-2021-delivery/target/quizz.zip"
 - depending on your OS edit (set the "DATA_STORAGE_FOLDER" variable to identify the data storage folder) then launch:
 	- quizz.sh if you use a Unix OS
 	- quizz.cmd if you use a Windows OS
 - Open a web browser, and open "http://localhost:8080"