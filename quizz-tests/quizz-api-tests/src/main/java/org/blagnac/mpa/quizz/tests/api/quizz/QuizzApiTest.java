package org.blagnac.mpa.quizz.tests.api.quizz;

import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.question.QuestionController;
import org.blagnac.mpa.quizz.quizz.QuizzController;
import org.blagnac.mpa.quizz.tests.api.question.QuestionApiTestConstants;
import org.blagnac.mpa.quizz.tests.common.api.ApiTest;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestUtils;

/**
 * Abstract Quizz API tests class
 * 
 * @author Thibault GOUBIN
 */
public abstract class QuizzApiTest extends ApiTest {

	/**
	 * Quizz API test requests folder path
	 */
	private static final String QUIZZ_API_TEST_REQUESTS = "quizzApiTest/";

	/**
	 * "src/test/resources/requests/question/post/quizzApiTest"
	 */
	private static final String INIT_QUESTION_DATA_REQUESTS_FOLDER = QuestionApiTestConstants.POST_QUESTION_REQUESTS_FOLDER
			+ QUIZZ_API_TEST_REQUESTS;

	/**
	 * "src/test/resources/requests/quizz/post/quizzApiTest"
	 */
	private static final String INIT_QUIZZ_DATA_REQUESTS_FOLDER = QuizzApiTestConstants.POST_QUIZZ_REQUESTS_FOLDER
			+ QUIZZ_API_TEST_REQUESTS;

	@Override
	public void initData() throws Exception {
		resetDataFile(DataStorageConstants.QUESTIONS_FILE);
		resetDataFile(DataStorageConstants.QUIZZES_FILE);
		
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_1");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_2");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_3");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_4");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_5");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_6");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_7");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_8");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_9");
		ApiTestUtils.post(mockMvc, resourceLoader, QuizzController.API_PATH,
				INIT_QUIZZ_DATA_REQUESTS_FOLDER + "initData_1");
		ApiTestUtils.post(mockMvc, resourceLoader, QuizzController.API_PATH,
				INIT_QUIZZ_DATA_REQUESTS_FOLDER + "initData_2");
		ApiTestUtils.post(mockMvc, resourceLoader, QuizzController.API_PATH,
				INIT_QUIZZ_DATA_REQUESTS_FOLDER + "initData_3");
	}
}
