package org.blagnac.mpa.quizz.tests.api.question;

import java.util.List;

import org.blagnac.mpa.quizz.question.Question;
import org.blagnac.mpa.quizz.question.QuestionController;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestUtils;
import org.hamcrest.Matchers;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Question API tests utilities
 * 
 * @author Thibault GOUBIN
 */
public class QuestionApiTestUtils {

	/**
	 * GET /question execution and check request execution success
	 * 
	 * @param mockMvc   the mock MVC
	 * @param questions the questions expected
	 * @throws Exception an exception
	 */
	public static void getQuestionsOK(MockMvc mockMvc, List<Question> questions) throws Exception {
		ResultActions resultActions = ApiTestUtils.get(mockMvc, QuestionController.API_PATH);
		resultActions.andExpect(MockMvcResultMatchers.status().isOk());

		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(questions.size())));
		for (int i = 0; i < questions.size(); i++) {
			verifyQuestionContent(resultActions, questions.get(i), "$[" + i + "]", false);
		}
	}

	/**
	 * POST /question execution and check request execution success
	 * 
	 * @param mockMvc        the mock MVC
	 * @param resourceLoader the resource loader
	 * @param requestFile    the JSON request file
	 * @param question       the expected question
	 * @throws Exception an exception
	 */
	public static void postQuestionOK(MockMvc mockMvc, ResourceLoader resourceLoader, String requestFile,
			Question question) throws Exception {
		ResultActions resultActions = ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				requestFile);
		resultActions.andExpect(MockMvcResultMatchers.status().isCreated());
		verifyQuestionContent(resultActions, question, "$", false);
	}

	/**
	 * Verifies the HTTP response content from a question object expected
	 * 
	 * @param resultActions        the result actions
	 * @param question             the expected question
	 * @param jsonPath             the JSON path
	 * @param correctAnswersHidden if the correct answers are hidden
	 * @throws Exception an exception
	 */
	public static void verifyQuestionContent(ResultActions resultActions, Question question, String jsonPath,
			boolean correctAnswersHidden) throws Exception {
		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".id").value(question.getId()));
		resultActions
				.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".category").value(question.getCategory().name()));
		resultActions.andExpect(
				MockMvcResultMatchers.jsonPath(jsonPath + ".difficulty").value(question.getDifficulty().name()));
		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".text").value(question.getText()));

		for (int i = 0; i < question.getAnswers().size(); i++) {
			resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".answers[" + i + "].text")
					.value(question.getAnswers().get(i).getText()));

			if (!correctAnswersHidden) {
				resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".answers[" + i + "].correct")
						.value(question.getAnswers().get(i).isCorrect()));
			} else {
				resultActions.andExpect(
						MockMvcResultMatchers.jsonPath(jsonPath + ".answers[" + i + "].correct").doesNotExist());
			}
		}
	}
}
