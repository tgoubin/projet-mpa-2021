package org.blagnac.mpa.quizz.tests.api.question;

import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.question.QuestionController;
import org.blagnac.mpa.quizz.tests.common.api.ApiTest;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestUtils;

/**
 * Abstract Question API tests class
 * 
 * @author Thibault GOUBIN
 */
public abstract class QuestionApiTest extends ApiTest {

	/**
	 * Question API test requests folder path
	 */
	private static final String QUESTION_API_TEST_REQUESTS = "questionApiTest/";

	/**
	 * "src/test/resources/requests/question/post/questionApiTest"
	 */
	private static final String INIT_QUESTION_DATA_REQUESTS_FOLDER = QuestionApiTestConstants.POST_QUESTION_REQUESTS_FOLDER
			+ QUESTION_API_TEST_REQUESTS;

	@Override
	public void initData() throws Exception {
		resetDataFile(DataStorageConstants.QUESTIONS_FILE);
		
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_1");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_2");
		ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				INIT_QUESTION_DATA_REQUESTS_FOLDER + "initData_3");
	}
}
