package org.blagnac.mpa.quizz.tests.api.question;

import java.util.Arrays;

import org.blagnac.mpa.quizz.question.Answer;
import org.blagnac.mpa.quizz.question.Question;
import org.blagnac.mpa.quizz.question.QuestionCategory;
import org.blagnac.mpa.quizz.question.QuestionDifficulty;
import org.blagnac.mpa.quizz.tests.common.QuestionFactory;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestConstants;

/**
 * Question API tests constants
 * 
 * @author Thibault GOUBIN
 */
public class QuestionApiTestConstants {

	/**
	 * POST /question requests data folder:
	 * "src/test/resources/requests/question/post"
	 */
	public static final String POST_QUESTION_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER + "/question/post/";

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/questionApiTest/initData_1.json"
	 */
	public static final Question QUESTION_API_TEST_POST_QUESTION_INIT_DATA_1 = QuestionFactory.createQuestion(1,
			"Question 1_1", QuestionCategory.HISTORY, QuestionDifficulty.EASY,
			Arrays.asList(new Answer("Answer 1_1_1", false), new Answer("Answer 1_1_2", false),
					new Answer("Answer 1_1_3", true), new Answer("Answer 1_1_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/questionApiTest/initData_2.json"
	 */
	public static final Question QUESTION_API_TEST_POST_QUESTION_INIT_DATA_2 = QuestionFactory.createQuestion(2,
			"Question 1_2", QuestionCategory.GEOGRAPHY, QuestionDifficulty.EASY,
			Arrays.asList(new Answer("Answer 1_2_1", false), new Answer("Answer 1_2_2", true),
					new Answer("Answer 1_2_3", false), new Answer("Answer 1_2_4", false)));

	/**?
	 * Question defined in
	 * "src/test/resources/requests/question/post/questionApiTest/initData_3.json"
	 */
	public static final Question QUESTION_API_TEST_POST_QUESTION_INIT_DATA_3 = QuestionFactory.createQuestion(3,
			"Question 1_3", QuestionCategory.MUSIC, QuestionDifficulty.MEDIUM,
			Arrays.asList(new Answer("Answer 1_3_1", false), new Answer("Answer 1_3_2", false),
					new Answer("Answer 1_3_3", false), new Answer("Answer 1_3_4", true)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_1.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_1 = QuestionFactory.createQuestion(1,
			"Question 1_1", QuestionCategory.HISTORY, QuestionDifficulty.EASY,
			Arrays.asList(new Answer("Answer 1_1_1", false), new Answer("Answer 1_1_2", false),
					new Answer("Answer 1_1_3", true), new Answer("Answer 1_1_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_2.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_2 = QuestionFactory.createQuestion(2,
			"Question 1_2", QuestionCategory.GEOGRAPHY, QuestionDifficulty.EASY,
			Arrays.asList(new Answer("Answer 1_2_1", false), new Answer("Answer 1_2_2", true),
					new Answer("Answer 1_2_3", false), new Answer("Answer 1_2_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_3.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_3 = QuestionFactory.createQuestion(3,
			"Question 1_3", QuestionCategory.MUSIC, QuestionDifficulty.MEDIUM,
			Arrays.asList(new Answer("Answer 1_3_1", false), new Answer("Answer 1_3_2", false),
					new Answer("Answer 1_3_3", false), new Answer("Answer 1_3_4", true)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_4.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_4 = QuestionFactory.createQuestion(4,
			"Question 2_1", QuestionCategory.SCIENCES, QuestionDifficulty.MEDIUM,
			Arrays.asList(new Answer("Answer 2_1_1", false), new Answer("Answer 2_1_2", true),
					new Answer("Answer 2_1_3", false), new Answer("Answer 2_1_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_5.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_5 = QuestionFactory.createQuestion(5,
			"Question 2_2", QuestionCategory.LITERATURE, QuestionDifficulty.EASY,
			Arrays.asList(new Answer("Answer 2_2_1", true), new Answer("Answer 2_2_2", false),
					new Answer("Answer 2_2_3", false), new Answer("Answer 2_2_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_6.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_6 = QuestionFactory.createQuestion(6,
			"Question 2_3", QuestionCategory.MUSIC, QuestionDifficulty.MEDIUM,
			Arrays.asList(new Answer("Answer 2_3_1", false), new Answer("Answer 2_3_2", false),
					new Answer("Answer 2_3_3", false), new Answer("Answer 2_3_4", true)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_7.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_7 = QuestionFactory.createQuestion(7,
			"Question 2_4", QuestionCategory.SPORT, QuestionDifficulty.HARD,
			Arrays.asList(new Answer("Answer 2_4_1", false), new Answer("Answer 2_4_2", false),
					new Answer("Answer 2_4_3", true), new Answer("Answer 2_4_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_8.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_8 = QuestionFactory.createQuestion(8,
			"Question 3_1", QuestionCategory.SCIENCES, QuestionDifficulty.HARD,
			Arrays.asList(new Answer("Answer 3_1_1", true), new Answer("Answer 3_1_2", false),
					new Answer("Answer 3_1_3", false), new Answer("Answer 3_1_4", false)));

	/**
	 * Question defined in
	 * "src/test/resources/requests/question/post/quizzApiTest/initData_9.json"
	 */
	public static final Question QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_9 = QuestionFactory.createQuestion(9,
			"Question 3_2", QuestionCategory.LITERATURE, QuestionDifficulty.EASY,
			Arrays.asList(new Answer("Answer 3_2_1", false), new Answer("Answer 3_2_2", false),
					new Answer("Answer 3_2_3", false), new Answer("Answer 3_2_4", true)));
}
