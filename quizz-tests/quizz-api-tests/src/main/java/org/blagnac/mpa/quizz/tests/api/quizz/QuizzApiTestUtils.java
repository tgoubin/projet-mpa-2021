package org.blagnac.mpa.quizz.tests.api.quizz;

import java.util.List;

import org.blagnac.mpa.quizz.quizz.Quizz;
import org.blagnac.mpa.quizz.quizz.QuizzController;
import org.blagnac.mpa.quizz.tests.api.question.QuestionApiTestUtils;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestUtils;
import org.hamcrest.Matchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Quizz API tests utilities
 * 
 * @author Thibault GOUBIN
 */
public class QuizzApiTestUtils {

	/**
	 * GET /quizz execution and check request execution success
	 * 
	 * @param mockMvc the mock MVC
	 * @param quizzes the quizzes expected
	 * @throws Exception an exception
	 */
	public static void getQuizzesOK(MockMvc mockMvc, List<Quizz> quizzes) throws Exception {
		ResultActions resultActions = ApiTestUtils.get(mockMvc, QuizzController.API_PATH);
		resultActions.andExpect(MockMvcResultMatchers.status().isOk());

		resultActions.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(quizzes.size())));
		for (int i = 0; i < quizzes.size(); i++) {
			verifyQuizzContent(resultActions, quizzes.get(i), i, true);
		}
	}

	/**
	 * Verifies the HTTP response content from a quizz object expected
	 * 
	 * @param resultActions        the result actions
	 * @param quizz                the expected quizz
	 * @param index                the index (optional) of the quizz in the HTTP
	 *                             response
	 * @param correctAnswersHidden if the correct answers are hidden
	 * @throws Exception an exception
	 */
	private static void verifyQuizzContent(ResultActions resultActions, Quizz quizz, Integer index,
			boolean correctAnswersHidden) throws Exception {
		String jsonPath = "$";
		if (index != null) {
			jsonPath += "[" + index + "]";
		}

		resultActions.andExpect(MockMvcResultMatchers.jsonPath(jsonPath + ".id").value(quizz.getId()));

		resultActions.andExpect(
				MockMvcResultMatchers.jsonPath(jsonPath + ".questions", Matchers.hasSize(quizz.getQuestions().size())));
		for (int i = 0; i < quizz.getQuestions().size(); i++) {
			QuestionApiTestUtils.verifyQuestionContent(resultActions, quizz.getQuestions().get(i),
					jsonPath + ".questions[" + i + "]", correctAnswersHidden);
		}
	}
}
