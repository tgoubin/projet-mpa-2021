package org.blagnac.mpa.quizz.tests.api.quizz;

import java.util.Arrays;

import org.blagnac.mpa.quizz.quizz.Quizz;
import org.blagnac.mpa.quizz.tests.api.question.QuestionApiTestConstants;
import org.blagnac.mpa.quizz.tests.common.QuizzFactory;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestConstants;

/**
 * Quizz API tests constants
 * 
 * @author Thibault GOUBIN
 */
public class QuizzApiTestConstants {

	/**
	 * POST /quizz requests data folder: "src/test/resources/requests/quizz/post"
	 */
	public static final String POST_QUIZZ_REQUESTS_FOLDER = ApiTestConstants.REQUESTS_FOLDER + "/quizz/post/";

	/**
	 * Quizz defined in
	 * "src/test/resources/requests/quizz/post/quizzApiTest/initData_1.json"
	 */
	public static final Quizz QUIZZ_API_TEST_POST_QUIZZ_INIT_DATA_1 = QuizzFactory.createQuizz(1,
			Arrays.asList(QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_1,
					QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_2,
					QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_3));

	/**
	 * Quizz defined in
	 * "src/test/resources/requests/quizz/post/quizzApiTest/initData_2.json"
	 */
	public static final Quizz QUIZZ_API_TEST_POST_QUIZZ_INIT_DATA_2 = QuizzFactory.createQuizz(2,
			Arrays.asList(QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_4,
					QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_5,
					QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_6,
					QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_7));

	/**
	 * Quizz defined in
	 * "src/test/resources/requests/quizz/post/quizzApiTest/initData_3.json"
	 */
	public static final Quizz QUIZZ_API_TEST_POST_QUIZZ_INIT_DATA_3 = QuizzFactory.createQuizz(3,
			Arrays.asList(QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_8,
					QuestionApiTestConstants.QUIZZ_API_TEST_POST_QUESTION_INIT_DATA_9));
}
