package org.blagnac.mpa.quizz.tests.api.quizz;

import java.util.Arrays;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * GET Quizz API tests
 * 
 * @author Thibault GOUBIN
 */
public class GetQuizzApiTest extends QuizzApiTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GetQuizzApiTest.class);

	/**
	 * GET /quizz - 200 I want to check that when I call the web service "GET
	 * /quizz" (all quizzes listing), the API response is "HTTP 200" (i.e. OK) and
	 * contains the expected quizzes
	 * 
	 * @throws Exception an exception
	 */
	@Test
	public void getQuizzes_200() throws Exception {
		LOGGER.info("GET /quizz - 200");

		QuizzApiTestUtils.getQuizzesOK(mockMvc,
				Arrays.asList(QuizzApiTestConstants.QUIZZ_API_TEST_POST_QUIZZ_INIT_DATA_1,
						QuizzApiTestConstants.QUIZZ_API_TEST_POST_QUIZZ_INIT_DATA_2,
						QuizzApiTestConstants.QUIZZ_API_TEST_POST_QUIZZ_INIT_DATA_3));
	}
}
