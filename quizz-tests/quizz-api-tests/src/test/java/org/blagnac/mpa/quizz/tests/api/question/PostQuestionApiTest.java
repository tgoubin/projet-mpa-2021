package org.blagnac.mpa.quizz.tests.api.question;

import java.util.Arrays;

import org.blagnac.mpa.quizz.question.Answer;
import org.blagnac.mpa.quizz.question.Question;
import org.blagnac.mpa.quizz.question.QuestionCategory;
import org.blagnac.mpa.quizz.question.QuestionController;
import org.blagnac.mpa.quizz.question.QuestionDifficulty;
import org.blagnac.mpa.quizz.tests.common.QuestionFactory;
import org.blagnac.mpa.quizz.tests.common.api.ApiTestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * POST Quizz API tests
 * 
 * @author Thibault GOUBIN
 */
public class PostQuestionApiTest extends QuestionApiTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PostQuestionApiTest.class);

	/**
	 * "src/test/resources/requests/question/post/postQuestionApiTest"
	 */
	private static final String REQUESTS_FOLDER = QuestionApiTestConstants.POST_QUESTION_REQUESTS_FOLDER
			+ "postQuestionApiTest/";

	/**
	 * POST /question - 201 : I want to check that when I call the web service "POST
	 * /quizz" (question creation), the API response is "HTTP 201" (i.e. OK) and
	 * contains the expected question created. I re-check the creation by calling
	 * "GET /question" (questions listing)
	 * 
	 * @throws Exception an exception
	 */
	@Test
	public void postQuestion_201() throws Exception {
		LOGGER.info("POST /question - 201");

		Question question = QuestionFactory.createQuestion(4, "POST Question 201", QuestionCategory.SCIENCES,
				QuestionDifficulty.HARD,
				Arrays.asList(new Answer("Answer POST Question 201 1", false),
						new Answer("Answer POST Question 201 2", true), new Answer("Answer POST Question 201 3", false),
						new Answer("Answer POST Question 201 4", false)));

		QuestionApiTestUtils.postQuestionOK(mockMvc, resourceLoader, REQUESTS_FOLDER + "postQuestion_201", question);

		QuestionApiTestUtils.getQuestionsOK(mockMvc,
				Arrays.asList(QuestionApiTestConstants.QUESTION_API_TEST_POST_QUESTION_INIT_DATA_1,
						QuestionApiTestConstants.QUESTION_API_TEST_POST_QUESTION_INIT_DATA_2,
						QuestionApiTestConstants.QUESTION_API_TEST_POST_QUESTION_INIT_DATA_3, question));
	}

	/**
	 * POST /question - 400 : I want to check that when I call the web service "POST
	 * /quizz" (question creation) with mandatory values missing, the API response
	 * is "HTTP 400". I re-check that no question is added by calling "GET
	 * /question" (questions listing)
	 * 
	 * @throws Exception an exception
	 */
	@Test
	public void postQuestion_400_MandatoryValues() throws Exception {
		LOGGER.info("POST /question - 400 - Mandatory values");

		postQuestion_400("postQuestion_400_textMissing", "The question text is mandatory");
		postQuestion_400("postQuestion_400_textEmpty", "The question text is mandatory");
		postQuestion_400("postQuestion_400_categoryMissing", "The question category is mandatory");
		postQuestion_400("postQuestion_400_difficultyMissing", "The question difficulty is mandatory");
	}

	/**
	 * POST /question - 400 : I want to check that when I call the web service "POST
	 * /quizz" (question creation) with bad answers definition, the API response is
	 * "HTTP 400". I re-check that no question is added by calling "GET /question"
	 * (questions listing)
	 * 
	 * @throws Exception an exception
	 */
	@Test
	public void postQuestion_400_BadAnswersDefinition() throws Exception {
		LOGGER.info("POST /question - 400 - Bad answers definition");

		postQuestion_400("postQuestion_400_noAnswer", "A question must have minimum 2 possible answers");
		postQuestion_400("postQuestion_400_lessThan2Answers", "A question must have minimum 2 possible answers");
		postQuestion_400("postQuestion_400_0CorrectAnswer", "A question must have 1 correct answer");
		postQuestion_400("postQuestion_400_2CorrectAnswers", "A question must have 1 correct answer");
		postQuestion_400("postQuestion_400_AnswerWithTextNull", "The answer text is mandatory");
		postQuestion_400("postQuestion_400_AnswerWithEmptyText", "The answer text is mandatory");
	}

	/**
	 * Execute a POST /question and check the HTTP 400 BadRequest error behaviour
	 * 
	 * @param requestFile          the request file
	 * @param expectedErrorMessage the expected error message
	 * @throws Exception an exception
	 */
	private void postQuestion_400(String requestFile, String expectedErrorMessage) throws Exception {
		ResultActions resultActions = ApiTestUtils.post(mockMvc, resourceLoader, QuestionController.API_PATH,
				REQUESTS_FOLDER + requestFile);
		resultActions = resultActions.andExpect(MockMvcResultMatchers.status().isBadRequest());
		Assert.assertEquals(resultActions.andReturn().getResponse().getContentAsString(), expectedErrorMessage);

		// Check that no question is added
		QuestionApiTestUtils.getQuestionsOK(mockMvc,
				Arrays.asList(QuestionApiTestConstants.QUESTION_API_TEST_POST_QUESTION_INIT_DATA_1,
						QuestionApiTestConstants.QUESTION_API_TEST_POST_QUESTION_INIT_DATA_2,
						QuestionApiTestConstants.QUESTION_API_TEST_POST_QUESTION_INIT_DATA_3));

	}
}
