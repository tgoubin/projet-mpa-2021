package org.blagnac.mpa.quizz.tests.common.unit;

/**
 * Unit tests constants
 * 
 * @author Thibault GOUBIN
 */
public class UnitTestConstants {

	/**
	 * Resource loader property
	 */
	public static final String RESOURCE_LOADER_PROPERTY = "resourceLoader";
}
