package org.blagnac.mpa.quizz.tests.common.unit;

import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.tests.common.GenericTest;
import org.mockito.MockitoAnnotations;

/**
 * Common abstract unit tests class
 * 
 * @author Thibault GOUBIN
 */
public abstract class UnitTest extends GenericTest {

	/**
	 * Mockito (mock engine) initialisation
	 * 
	 * @throws Exception an exception
	 */
	@Override
	public void init() throws Exception {
		MockitoAnnotations.initMocks(this);
		initData();

		// The data file are in "src/test/resources/data/"
		DataStorageConstants.DATA_STORAGE_PATH = DataStorageConstants.TESTS_DATA_STORAGE_PATH;
	}
}
