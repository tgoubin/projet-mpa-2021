package org.blagnac.mpa.quizz.tests.common;

import java.util.List;

import org.blagnac.mpa.quizz.question.Question;
import org.blagnac.mpa.quizz.quizz.Quizz;

/**
 * Factory for Quizz objects
 * 
 * @author Thibault GOUBIN
 */
public class QuizzFactory {

	/**
	 * Creates a Quizz object
	 * 
	 * @param id        the id
	 * @param questions the questions
	 * @return the Quizz object
	 */
	public static Quizz createQuizz(Integer id, List<Question> questions) {
		Quizz quizz = new Quizz();
		quizz.setId(id);
		quizz.setQuestions(questions);
		return quizz;
	}
}
