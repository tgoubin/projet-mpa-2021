package org.blagnac.mpa.quizz.tests.common.api;

import java.io.IOException;

import org.blagnac.mpa.quizz.QuizzApplication;
import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.common.FileUtils;
import org.blagnac.mpa.quizz.tests.common.GenericTest;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Common abstract API tests class
 * 
 * @author Thibault GOUBIN
 */
@SpringBootTest(classes = { QuizzApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public abstract class ApiTest extends GenericTest {

	/**
	 * API tests engine
	 */
	@Autowired
	protected MockMvc mockMvc;

	/**
	 * Resource loader
	 */
	@Autowired
	protected ResourceLoader resourceLoader;

	/**
	 * JSON mapper and data initialization
	 * 
	 * @throws Exception an exception
	 */
	@Override
	public void init() throws Exception {
		ApiTestUtils.initJSONMapper();
		initData();
	}

	/**
	 * Reset a data file
	 * 
	 * @param dataFileName the data file name
	 * @throws IOException an IO exception
	 */
	protected void resetDataFile(String dataFileName) throws IOException {
		FileUtils.writeInFile(DataStorageConstants.DATA_STORAGE_PATH + dataFileName,
				DataStorageConstants.EMPTY_FILE_CONTENT, false);
	}

	/**
	 * Resets all data files
	 * 
	 * @throws IOException an IO exception
	 */
	@After
	public void resetAll() throws IOException {
		resetDataFile(DataStorageConstants.RESULTS_FILE);
		resetDataFile(DataStorageConstants.QUESTIONS_FILE);
		resetDataFile(DataStorageConstants.QUIZZES_FILE);
	}
}
