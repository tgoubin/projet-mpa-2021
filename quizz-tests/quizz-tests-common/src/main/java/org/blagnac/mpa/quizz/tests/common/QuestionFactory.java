package org.blagnac.mpa.quizz.tests.common;

import java.util.List;

import org.blagnac.mpa.quizz.question.Answer;
import org.blagnac.mpa.quizz.question.Question;
import org.blagnac.mpa.quizz.question.QuestionCategory;
import org.blagnac.mpa.quizz.question.QuestionDifficulty;

/**
 * Factory for Question objects
 * 
 * @author Thibault GOUBIN
 */
public class QuestionFactory {

	/**
	 * Creates a Question object
	 * 
	 * @param id         the id
	 * @param text       the text
	 * @param category   the category
	 * @param difficulty the difficulty
	 * @param answers    the answers
	 * @return the Question object
	 */
	public static Question createQuestion(Integer id, String text, QuestionCategory category,
			QuestionDifficulty difficulty, List<Answer> answers) {
		Question question = new Question(text, category, difficulty, answers);
		question.setId(id);
		return question;
	}
}
