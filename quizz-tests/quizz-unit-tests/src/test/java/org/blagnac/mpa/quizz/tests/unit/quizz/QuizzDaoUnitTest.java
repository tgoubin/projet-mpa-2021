package org.blagnac.mpa.quizz.tests.unit.quizz;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.blagnac.mpa.quizz.quizz.Quizz;
import org.blagnac.mpa.quizz.quizz.QuizzDao;
import org.blagnac.mpa.quizz.tests.common.unit.UnitTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit tests for 'QuizzDao'
 * 
 * @author Thibault GOUBIN
 */
public class QuizzDaoUnitTest extends UnitTest {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuizzDaoUnitTest.class);

	/**
	 * DAO to test
	 */
	@InjectMocks
	private QuizzDao quizzDao;

	/**
	 * Unit test for 'QuizzDao::findContainingQuestion()' - OK
	 * 
	 * @throws IOException an IO exception
	 */
	@Test
	public void findContainingQuestion_OK() throws IOException {
		LOGGER.info("Unit test for 'QuizzDao::findContainingQuestion()' - OK");

		// Method to test execution
		List<Quizz> quizzes = quizzDao.findContainingQuestion(1);

		// Quizzes ids retrieving
		List<Integer> quizzesIds = quizzes.stream().map(q -> q.getId()).collect(Collectors.toList());

		// I check that 2 quizzes contain the question with id=1
		Assert.assertTrue(quizzes.size() == 2);
		// I check that there is the quizz with id=1
		Assert.assertTrue(quizzesIds.contains(1));
		// I check that there is the quizz with id=2
		Assert.assertTrue(quizzesIds.contains(2));
	}
}
