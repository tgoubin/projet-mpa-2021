#!bin/sh

DATA_STORAGE_FOLDER="/tmp/quizz"

if [ ! -d "$DATA_STORAGE_FOLDER" ] then
    mkdir $DATA_STORAGE_FOLDER
fi

java -jar .\bin\quizz-spring-boot.jar $DATA_STORAGE_FOLDER