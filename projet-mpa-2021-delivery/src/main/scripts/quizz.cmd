set DATA_STORAGE_FOLDER=C:/quizz

if not exist %DATA_STORAGE_FOLDER% (
	mkdir %DATA_STORAGE_FOLDER%
)

java -jar .\bin\quizz-spring-boot.jar %DATA_STORAGE_FOLDER%