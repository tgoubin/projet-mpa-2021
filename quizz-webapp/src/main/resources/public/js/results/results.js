window.onload = function() {
	ResultsController.load_Results();
};

ResultsController = {};

ResultsController.load_Results = () => {
	const httpRequest = playerApiPath + '/results';
	fetch(httpRequest).then(response => {
		if (response.ok) {
			return response.json();
		} else {
			throw response.text();
		}
	}).then(results => {
		let resultsProcessed = [];
		for (let i in results) {
			const resultProcessed = {};
			const dateTime = new Date(results[i].dateTime);
			resultProcessed.quizzId = '#' + results[i].quizzId;
			resultProcessed.playerName = results[i].playerName;
			resultProcessed.dateTime = dateTime.getDate() +
				'/' + (dateTime.getMonth() + 1) +
				'/' + dateTime.getFullYear() +
				' ' + dateTime.getHours() +
				':' + dateTime.getMinutes();
			resultProcessed.score = results[i].score;
			resultProcessed.percentage = (Math.round((results[i].score / results[i].nbQuestions) * 10000) / 100).toString() + ' %';
			resultsProcessed.push(resultProcessed);
		}

		new Tabulator('#container-results', {
			data: resultsProcessed,
			height: 800,
			columns: [
				{
					title: 'Quizz',
					field: 'quizzId',
					width: 100,
					headerFilter: true
				},
				{
					title: 'Name',
					field: 'playerName',
					width: 250,
					headerFilter: true
				},
				{
					title: 'Date / Time',
					field: 'dateTime',
					width: 150,
				},
				{
					title: 'Score',
					field: 'score',
					width: 100
				},
				{
					title: 'Percentage',
					field: 'percentage',
					width: 200
				}
			]
		});
	}).catch(responseError => {
		console.log(responseError);
		responseError.then(error => {
			alert('GET ' + httpRequest + ' error !\n\n' + error);
		});
	});
};