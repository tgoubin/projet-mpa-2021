AdminQuizzController = {};

AdminQuizzController.buildQuizzObject = (quizz) => {
	alert('NOT IMPLEMENTED');
};

AdminQuizzController.load_Quizzes = () => {
	fetch(quizzApiPath).then(response => {
			if (response.ok) {
				return response.json();
			} else {
				throw response.text();
			}
		}
	).then(quizzes => {
			const existingTBody = document.querySelector('#list-quizzes .container-table table tbody');
			if (existingTBody) {
				existingTBody.remove();
			}

			const tbody = document.createElement('tbody');

			for (let i in quizzes) {
				const tr = document.createElement('tr');
				AdminController.createTextCell(quizzes[i].id, tr);
				AdminController.createTextCell(quizzes[i].questions.length, tr);
				AdminController.createButtonCell('Edit', 'bt-edit-quizz', quizzes[i].id, tr);
				AdminController.createButtonCell('Delete', 'bt-delete-quizz', quizzes[i].id, tr);
				tbody.appendChild(tr);
			}

			document.querySelector('#list-quizzes .container-table table').appendChild(tbody);

			onClick_btEditQuizz();
			onClick_btDeleteQuizz();
		}
	).catch(responseError => {
		responseError.then(error => {
			alert('GET ' + quizzApiPath + ' error !\n\n' + error);
		});
	});

	function onClick_btEditQuizz() {
		document.querySelectorAll('button.bt-edit-quizz').forEach(button => {
			button.onclick = () => {
				AdminController.control_FormsEdit('quizz', button.getAttribute('id-element'));
			};
		});
	}

	function onClick_btDeleteQuizz() {
		document.querySelectorAll('button.bt-delete-quizz').forEach(button => {
			button.onclick = () => {
				if (confirm('Do you really want to delete this quizz?')) {
					const httpRequest = quizzApiPath + '/' + button.getAttribute('id-element');
					fetch(httpRequest, {
						method: 'DELETE'
					}).then(response => {
							if (response.ok) {
								AdminController.load_Quizzes();
							} else {
								throw response.text();
							}
						}
					).catch(responseError => {
						responseError.then(error => {
							alert('DELETE ' + httpRequest + ' error !\n\n' + error);
						});
					});
				}
			};
		});
	}
};