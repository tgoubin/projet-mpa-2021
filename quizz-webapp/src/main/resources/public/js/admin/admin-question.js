AdminQuestionController = {};

AdminQuestionController.control_QuestionForm = (text, category, difficulty, answers) => {
	document.getElementById('input-question-text').value = text;
	loadElementsInSelect('category', category);
	loadElementsInSelect('difficulty', difficulty);

	if (answers) {
		resetFormEditQuestion();

		document.getElementById('input-question-answer-1').value = answers[0].text;
		document.getElementById('input-question-answer-2').value = answers[1].text;

		if (answers[0].correct) {
			document.getElementById('input-question-answer-1-correct').checked = true;
		}
		if (answers[1].correct) {
			document.getElementById('input-question-answer-2-correct').checked = true;
		}

		for (let i = 2; i < answers.length; i++) {
			addAnswerLine(parseInt(i) + 1, answers[i].text, answers[i].correct);
		}
	} else {
		document.getElementById('input-question-answer-1').value = '';
		document.getElementById('input-question-answer-1-correct').checked = false;
		document.getElementById('input-question-answer-2').value = '';
		document.getElementById('input-question-answer-2-correct').checked = false;

		resetFormEditQuestion();
	}

	onClick_btAddAnswer();

	function resetFormEditQuestion() {
		document.querySelectorAll('#form-edit-question .container-inputs .inputs-line-answer[permanent="false"]').forEach(answerDivNonPermanent => {
			answerDivNonPermanent.remove();
		});
	}

	function loadElementsInSelect(elementsClass, elementToSelect) {
		const httpRequest = questionApiPath + '/' + elementsClass;
		fetch(httpRequest).then(response => {
				if (response.ok) {
					return response.json();
				} else {
					throw response.text();
				}
			}
		).then(elementsList => {
				document.getElementById('input-question-' + elementsClass).innerHTML = '';
				const option0 = document.createElement('option');
				option0.text = '';
				option0.value = '';
				document.getElementById('input-question-' + elementsClass).appendChild(option0);
				for (let i in elementsList) {
					const option = document.createElement('option');
					option.value = elementsList[i];
					option.text = elementsList[i];
					document.getElementById('input-question-' + elementsClass).appendChild(option);

					if (elementToSelect === elementsList[i]) {
						option.selected = true;
					}
				}
			}
		).catch(responseError => {
			responseError.then(error => {
				alert('GET ' + httpRequest + ' error !\n\n' + error);
			});
		});
	}

	function onClick_btAddAnswer() {
		document.getElementById('bt-add-answer').onclick = () => {
			const numAnswer = document.querySelectorAll('#form-edit-question .container-inputs .inputs-line-answer').length + 1;
			addAnswerLine(numAnswer, '', false);
		};
	}

	function addAnswerLine(numAnswer, text, isCorrect) {
		const inputsLine = document.createElement('div');
		inputsLine.classList = 'inputs-line inputs-line-answer';
		inputsLine.id = 'inputs-line-answer-' + numAnswer;
		inputsLine.setAttribute('permanent', 'false');
		let htmlCode = '<div class="input-label">Answer ' + numAnswer + ':</div>';
		htmlCode += '<div class="input-item input-item-question-answer" id="input-item-question-answer-' + numAnswer + '">';
		htmlCode += '<input type="text" class="input-question-answer" id="input-question-answer-' + numAnswer + '" value="' + text + '">';
		htmlCode += '</div>';
		htmlCode += '<div class="input-label">Correct?</div>';
		htmlCode += '<div class="input-item" id="input-item-question-answer-' + numAnswer + '-correct">';

		const checked = (isCorrect) ? ' checked="checked"' : '';
		htmlCode += '<input type="radio" id="input-question-answer-' + numAnswer + '-correct" name="answer-correct" value="' + numAnswer + '"' + checked + '>';
		htmlCode += '</div>';
		inputsLine.innerHTML = htmlCode;
		document.querySelector('#form-edit-question .container-inputs').appendChild(inputsLine);
	}
};

AdminQuestionController.buildQuestionObject = (question) => {
	question.text = document.getElementById('input-question-text').value;
	question.category =
		(document.getElementById('input-question-category').value !== '')
			? document.getElementById('input-question-category').value : null;
	question.difficulty =
		(document.getElementById('input-question-difficulty').value !== '')
			? document.getElementById('input-question-difficulty').value : null;
	question.answers = [];
	for (let i = 1; i <= document.querySelectorAll('#form-edit-question .container-inputs .inputs-line-answer').length; i++) {
		const answer = {};
		answer.text = document.getElementById('input-question-answer-' + i).value;
		answer.correct = document.getElementById('input-question-answer-' + i + '-correct').checked;
		question.answers.push(answer);
	}
};

AdminQuestionController.load_Questions = () => {
	fetch(questionApiPath).then(response => {
			if (response.ok) {
				return response.json();
			} else {
				throw response.text();
			}
		}
	).then(questions => {
			const existingTBody = document.querySelector('#list-questions .container-table table tbody');
			if (existingTBody) {
				existingTBody.remove();
			}

			const tbody = document.createElement('tbody');

			for (let i in questions) {
				const tr = document.createElement('tr');
				AdminController.createTextCell(questions[i].text, tr);
				AdminController.createTextCell(questions[i].category, tr);
				AdminController.createTextCell(questions[i].difficulty, tr);
				AdminController.createButtonCell('Edit', 'bt-edit-question', questions[i].id, tr);
				AdminController.createButtonCell('Delete', 'bt-delete-question', questions[i].id, tr);
				tbody.appendChild(tr);
			}

			document.querySelector('#list-questions .container-table table').appendChild(tbody);

			onClick_btEditQuestion();
			onClick_btDeleteQuestion();
		}
	).catch(responseError => {
		responseError.then(error => {
			alert('GET ' + questionApiPath + ' error !\n\n' + error);
		});
	});

	function onClick_btEditQuestion() {
		document.querySelectorAll('button.bt-edit-question').forEach(button => {
			button.onclick = () => {
				AdminController.control_FormsEdit('question', button.getAttribute('id-element'));
			};
		});
	}

	function onClick_btDeleteQuestion() {
		document.querySelectorAll('button.bt-delete-question').forEach(button => {
			button.onclick = () => {
				if (confirm('Do you really want to delete this question?')) {
					const httpRequest = questionApiPath + '/' + button.getAttribute('id-element');
					fetch(httpRequest, {
						method: 'DELETE'
					}).then(response => {
							if (response.ok) {
								AdminController.load_Quizzes();
								AdminController.load_Questions();
							} else {
								throw response.text();
							}
						}
					).catch(responseError => {
						responseError.then(error => {
							alert('DELETE ' + httpRequest + ' error !\n\n' + error);
						});
					});
				}
			};
		});
	}
};