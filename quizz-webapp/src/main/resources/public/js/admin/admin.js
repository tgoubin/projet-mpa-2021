window.onload = function() {
	AdminController.onClick_btCreate();
	AdminController.onClick_btEditDataFile();
	AdminController.load_Quizzes();
	AdminController.load_Questions();
};

AdminController = {};

AdminController.onClick_btCreate = () => {
	document.querySelectorAll('button.bt-create').forEach(button => {
		button.onclick = () => {
			AdminController.control_FormsEdit(button.getAttribute('topic'), null);
		};
	});
};

AdminController.onClick_btEditDataFile = () => {
	document.querySelectorAll('button.bt-edit-data-file').forEach(button => {
		button.onclick = () => {
			AdminController.control_FormsEdit(button.getAttribute('topic'), null, true);
		};
	});
};

AdminController.control_FormsEdit = (topic, idElement, isDataFile) => {
	document.querySelectorAll('.form-edit').forEach(form => {
		form.style.display = 'none';
	});

	if (isDataFile) {
		control_FormEditDataFile(topic);
	} else {
		control_FormEdit(topic, idElement);
	}

	function control_FormEdit(topic, idElement) {
		const topicLabel = getTopicLabel(topic);
		
		if (idElement) {
			const httpRequest = apiPath + '/' + topic + '/' + idElement;
			fetch(httpRequest).then(response => {
					if (response.ok) {
						return response.json();
					} else {
						throw response.text();
					}
				}
			).then(element => {
					document.getElementById('form-edit-' + topic).style.display = 'block';

					document.querySelector('#form-edit-' + topic + ' h1').innerHTML = 'Edit the ' + topic + ' \'#' + idElement + '\'';

					if (topic === 'question') {
						AdminQuestionController.control_QuestionForm(element.text, element.category, element.difficulty, element.answers);
					} else if (topic === 'quizz') {
						// TODO Not implemented
					}

					onClick_btFormEdit(topic, idElement);
				}
			).catch(responseError => {
				responseError.then(error => {
					alert('GET ' + httpRequest + ' error !\n\n' + error);
				});
			});
		} else {
			document.getElementById('form-edit-' + topic).style.display = 'block';
			document.querySelector('#form-edit-' + topic + ' h1').innerHTML = 'Create a ' + topic;

			if (topic === 'question') {
				AdminQuestionController.control_QuestionForm('', null, null, null);
			} else if (topic === 'quizz') {
				// TODO Not implemented
			}

			onClick_btFormEdit(topic, null);
		}

		function onClick_btFormEdit(topic, idElement) {
			document.getElementById('bt-validate-edit-' + topic).onclick = () => {
				let httpRequest = apiPath + '/' + topic;
				let httpMethod = 'POST';

				if (idElement) {
					httpRequest += '/' + idElement;
					httpMethod = 'PUT';
				}

				const elementToEdit = {};
				elementToEdit.id = (idElement) ? idElement : null;
				if (topic === 'question') {
					AdminQuestionController.buildQuestionObject(elementToEdit);
				} else if (topic === 'quizz') {
					AdminQuizzController.buildQuizzObject(elementToEdit);
				}

				fetch(httpRequest, {
					method: httpMethod,
					headers: {
						'content-type': 'application/json'
					},
					body: JSON.stringify(elementToEdit)
				}).then(response => {
						if (response.ok) {
							document.getElementById('form-edit-question').style.display = 'none';
							AdminController['load_' + topicLabel]();
							alert(topic[0].toUpperCase() + topic.slice(1) + ' saved successfully');
						} else {
							throw response.text();
						}
					}
				).catch(responseError => {
					responseError.then(error => {
						alert(httpMethod + ' ' + httpRequest + ' error !\n\n' + error);
					});
				});
			};
		}
	}

	function control_FormEditDataFile(topic) {
		const topicLabel = getTopicLabel(topic);

		const httpRequest = apiPath + '/' + topic + '/storagefile';
		fetch(httpRequest).then(
			(response) => {
				if (response.ok) {
					return response.text();
				} else {
					throw response.text();
				}
			}
		).then(storageFileContent => {
				document.getElementById('form-edit-storage-file').style.display = 'block';

				document.querySelector('#form-edit-storage-file h1').innerHTML = 'Edit the ' + topicLabel + ' data file';
				document.querySelector('#form-edit-storage-file .container-inputs textarea').value = storageFileContent;

				onClick_btFormEditStorageFile(topic);
			}
		).catch(responseError => {
			responseError.then(error => {
				alert('GET ' + httpRequest + ' error !\n\n' + error);
			});
		});

		function onClick_btFormEditStorageFile(topic) {
			document.querySelector('#form-edit-storage-file button').onclick = () => {
				const httpRequest = apiPath + '/' + topic + '/storagefile';
				fetch(httpRequest, {
					method: 'POST',
					headers: {
						'content-type': 'text/plain'
					},
					body: document.querySelector('#form-edit-storage-file .container-inputs textarea').value
				}).then(response => {
						console.log(response);
						console.log(response.ok);
						alert(response.ok);
						if (response.ok) {
							document.getElementById('form-edit-storage-file').style.display = 'none';
							AdminController['load_' + topicLabel]();
							alert(topicLabel + ' storage file updated successfully');
						} else {
							throw response.text();
						}
					}
				).catch(responseError => {
					responseError.then(error => {
						alert('POST ' + httpRequest + ' error !\n\n' + error);
					});
				});
			};
		}
	}

	function getTopicLabel(topic) {
		let label = '';

		if (topic === 'question') {
			label = 'Questions';
		} else if (topic === 'quizz') {
			label = 'Quizzes';
		}

		return label;
	}
};

AdminController.load_Quizzes = () => {
	AdminQuizzController.load_Quizzes();
};

AdminController.load_Questions = () => {
	AdminQuestionController.load_Questions();
};

AdminController.createTextCell = (questionField, tr) => {
	const td = document.createElement('td');
	td.appendChild(document.createTextNode(questionField));
	tr.appendChild(td);
};

AdminController.createButtonCell = (btText, btClass, elementId, tr) => {
	const td = document.createElement('td');
	const button = document.createElement('button');
	button.classList = btClass;
	button.innerHTML = btText;
	button.setAttribute('id-element', elementId);
	td.appendChild(button);
	tr.appendChild(td);
};