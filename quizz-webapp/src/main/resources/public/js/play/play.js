window.onload = function() {
	PlayController.load_Quizzes();
};

PlayController = {};

PlayController.load_Quizzes = () => {
	fetch(quizzApiPath).then(response => {
		if (response.ok) {
			return response.json();
		} else {
			throw response.text();
		}
	}).then(quizzes => {
		const option0 = document.createElement('option');
		option0.text = '';
		option0.value = '';
		document.getElementById('input-quizz').appendChild(option0);
		for (let i in quizzes) {
			const option = document.createElement('option');
			option.value = quizzes[i].id;
			option.text = quizzes[i].stringDisplay;
			document.getElementById('input-quizz').appendChild(option);
		}

		onClick_btPlayQuizz();
	}).catch(responseError => {
		responseError.then(error => {
			alert('GET ' + quizzApiPath + ' error !\n\n' + error);
		});
	});

	function onClick_btPlayQuizz() {
		document.getElementById('bt-play-quizz').onclick = () => {
			const quizzId = document.getElementById('input-quizz').value;

			if (quizzId) {
				const name = prompt('What\'s your name?');

				if (name !== '') {
					if (name) {
						launchQuizz(quizzId, name);
					}
				} else {
					alert('The player name is mandatory !');
				}
			} else {
				alert('Please select a quizz !');
			}
		};
	}

	function launchQuizz(quizzId, name) {
		document.getElementById('quizz-player').style.display = 'block';

		const httpRequest = quizzApiPath + '/' + quizzId;
		fetch(httpRequest).then(response => {
			if (response.ok) {
				return response.json();
			} else {
				throw response.text();
			}
		}
		).then(quizz => {
			displayQuestions(quizz.questions);
			onClick_btValidateAnswers(quizzId, name);
		}
		).catch(responseError => {
			responseError.then(error => {
				alert('GET ' + httpRequest + ' error !\n\n' + error);
			});
		});
	}

	function displayQuestions(questions) {
		document.getElementById('quizz-questions').innerHTML = '';
		for (let i in questions) {
			const divQuestion = document.createElement('div');
			divQuestion.classList = 'quizz-question';
			divQuestion.setAttribute('question-id', questions[i].id);

			let questionHTML = '<div class="question-text">' + questions[i].text + '</div>';

			for (let j in questions[i].answers) {
				questionHTML += '<div class="answer">';
				questionHTML += '<div class="answer-text">' + questions[i].answers[j].text + '</div>';
				questionHTML += '<div class="answer-input"><input type="radio" name="answer-question-' + i + '" value="' + j + '"></div>';
				questionHTML += '</div>';
			}

			divQuestion.innerHTML = questionHTML;
			document.getElementById('quizz-questions').appendChild(divQuestion);
		}
	}

	function onClick_btValidateAnswers(quizzId, name) {
		document.getElementById('bt-validate-answers').onclick = () => {
			if (document.querySelectorAll('input[type=radio]:checked').length !== document.querySelectorAll('#quizz-player #quizz-questions .quizz-question').length) {
				alert('Please answer all the questions !');
			} else {
				const questionsAnswers = {};
				document.querySelectorAll('#quizz-player #quizz-questions .quizz-question').forEach(quizzQuestionDiv => {
					const questionId = quizzQuestionDiv.getAttribute('question-id');
					const answerValue = quizzQuestionDiv.querySelector('input[type=radio]:checked').value;
					questionsAnswers[questionId] = answerValue;
				});

				const playerQuestionsAnswers = {
					quizzId: quizzId,
					playerName: name,
					questionsAnswers: questionsAnswers
				};
				const httpRequest = playerApiPath + '/validatequizz';
				fetch(httpRequest, {
					method: 'POST',
					headers: {
						'content-type': 'application/json'
					},
					body: JSON.stringify(playerQuestionsAnswers)
				}).then(response => {
					if (response.ok) {
						return response.json();
					} else {
						throw response.text();
					}
				}).then(score => {
					alert('Your score is ' + score + ' / ' + document.querySelectorAll('#quizz-player #quizz-questions .quizz-question').length);
					window.location.reload();
				}).catch(responseError => {
					responseError.then(error => {
						alert('POST ' + httpRequest + ' error !\n\n' + error);
					});
				});
			}
		};
	}
};