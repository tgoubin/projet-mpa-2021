package org.blagnac.mpa.quizz;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.blagnac.mpa.quizz.common.ConfigurationConstants;
import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.common.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Runner component for Quizz REST SpringBoot Application
 * 
 * @author Thibault GOUBIN
 */
@Component
public class QuizzApplicationRunnerComponent implements ApplicationRunner {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuizzApplicationRunnerComponent.class);

	/**
	 * The server context path
	 */
	@Value(ConfigurationConstants.SERVER_CONTEXT_PATH)
	private String serverContextPath;

	/**
	 * The server port
	 */
	@Value(ConfigurationConstants.SERVER_PORT)
	private String serverPort;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// Data storage files saving
		if (args.getSourceArgs().length > 0) {
			DataStorageConstants.DATA_STORAGE_PATH = args.getSourceArgs()[0];
			if (!DataStorageConstants.DATA_STORAGE_PATH.endsWith("/")) {
				DataStorageConstants.DATA_STORAGE_PATH += "/";
			}
		} else {
			// If no argument, this is the API test mode
			DataStorageConstants.DATA_STORAGE_PATH = DataStorageConstants.TESTS_DATA_STORAGE_PATH;
		}

		checkDataStorageFolder();
		checkDataStorageFiles();

		// Ignore null values in JSON
		DataStorageConstants.JSON_MAPPER.setSerializationInclusion(Include.NON_EMPTY);

		LOGGER.info("The Quizz API ready. The web UI is accessible at 'http://localhost:{}{}'", serverPort,
				serverContextPath);
	}

	/**
	 * Checks the data storage folder
	 */
	private static void checkDataStorageFolder() {
		LOGGER.info("Data storage folder checking...");

		Path dataStoragePath = Paths.get(DataStorageConstants.DATA_STORAGE_PATH);
		if (Files.notExists(dataStoragePath)) {
			LOGGER.error("The folder '{}' does not exist...", DataStorageConstants.DATA_STORAGE_PATH);
			System.exit(1);
		}

		if (!Files.isDirectory(dataStoragePath)) {
			LOGGER.error("'{}' is not a folder...", DataStorageConstants.DATA_STORAGE_PATH);
			System.exit(1);
		}
	}

	/**
	 * Checks the data storage files
	 */
	private void checkDataStorageFiles() {
		LOGGER.info("Data storage files checking...");

		for (String dataStorageFile : DataStorageConstants.DATA_STORAGE_FILES) {
			String dataStorageFilePath = DataStorageConstants.DATA_STORAGE_PATH + dataStorageFile;
			if (!new File(dataStorageFilePath).exists()) {
				try {
					LOGGER.info("Data storage file '{}' does not exist", dataStorageFilePath);
					LOGGER.info("Data storage file '{}' initialization", dataStorageFilePath);
					new File(dataStorageFilePath).createNewFile();
					FileUtils.writeInFile(dataStorageFilePath, DataStorageConstants.EMPTY_FILE_CONTENT, false);
				} catch (IOException e) {
					LOGGER.error("Error during '{}' initialization", dataStorageFilePath);
					System.exit(1);
				}
			} else {
				LOGGER.info("Data storage file '{}' is present", dataStorageFilePath);
			}
		}
	}
}
