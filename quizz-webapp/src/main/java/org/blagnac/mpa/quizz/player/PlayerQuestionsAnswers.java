package org.blagnac.mpa.quizz.player;

import java.util.Map;

/**
 * Description class for couple Question / Answer
 * 
 * @author Thibault GOUBIN
 */
public class PlayerQuestionsAnswers {

	/**
	 * The Quizz id
	 */
	private Integer quizzId;

	/**
	 * The player name
	 */
	private String playerName;

	/**
	 * The Question id
	 */
	private Map<Integer, Integer> questionsAnswers;

	public Integer getQuizzId() {
		return quizzId;
	}

	public void setQuizzId(Integer quizzId) {
		this.quizzId = quizzId;
	}

	public Map<Integer, Integer> getQuestionsAnswers() {
		return questionsAnswers;
	}

	public void setQuestionsAnswers(Map<Integer, Integer> questionsAnswers) {
		this.questionsAnswers = questionsAnswers;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
}
