package org.blagnac.mpa.quizz.quizz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.blagnac.mpa.quizz.common.AbstractDao;
import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object for Quizz
 * 
 * @author Thibault GOUBIN
 */
@Component
public class QuizzDao extends AbstractDao<Quizz> {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuizzDao.class);

	@Override
	public List<Quizz> findAll() throws IOException {
		return findAll(new TypeReference<List<Quizz>>() {
		});
	}

	@Override
	public String dataFileName() {
		return DataStorageConstants.QUIZZES_FILE;
	}

	/**
	 * Finds the quizzes containing a question
	 * 
	 * @param questionId the question id
	 * @return the quizzes containing the question
	 * @throws IOException an IO exception
	 */
	public List<Quizz> findContainingQuestion(Integer questionId) throws IOException {
		List<Quizz> quizzes = findAll();
		List<Quizz> quizzesContainingQuestion = new ArrayList<>();

		for (Quizz quizz : quizzes) {
			if (quizz.getQuestions().stream().filter(q -> q.getId() == questionId).count() > 0) {
				quizzesContainingQuestion.add(quizz);
			}
		}

		LOGGER.info("{} quizzes contain question with id '{}'", quizzesContainingQuestion.size(), questionId);
		return quizzesContainingQuestion;
	}
}
