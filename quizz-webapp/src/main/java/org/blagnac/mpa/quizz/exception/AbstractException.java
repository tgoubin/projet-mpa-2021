package org.blagnac.mpa.quizz.exception;

/**
 * Abstract class for all exceptions
 * 
 * @author Thibault GOUBIN
 */
public abstract class AbstractException extends RuntimeException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -4239220262784620908L;

	/**
	 * The error message
	 */
	private final String errorMessage;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public AbstractException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	/**
	 * HTTP code corresponding to the error
	 * 
	 * @return the HTTP code
	 */
	public abstract int getHttpStatusCode();

	public String getErrorMessage() {
		return errorMessage;
	}
}
