package org.blagnac.mpa.quizz.quizz;

import java.util.List;

import org.blagnac.mpa.quizz.common.ApiConstants;
import org.blagnac.mpa.quizz.exception.AbstractException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * REST controller for Quizz
 * 
 * @author Thibault GOUBIN
 */
@Controller
@RequestMapping(value = QuizzController.API_PATH)
public class QuizzController {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuizzController.class);

	/**
	 * API path
	 */
	public static final String API_PATH = ApiConstants.API_BASE_PATH + "/quizz";

	/**
	 * Service for quizz objects
	 */
	@Autowired
	private QuizzService quizzService;

	/**
	 * GET api/quizz - quizzes listing
	 * 
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity getQuizzes() {
		LOGGER.info("GET api/quizz");

		try {
			return new ResponseEntity<List<Quizz>>(quizzService.getAllQuizzes(), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * GET api/quizz/{id} - quizz by id
	 * 
	 * @param id    the quizz id
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity getQuizzById(@PathVariable(value = "id") Integer id) {
		LOGGER.info("GET api/quizz/{}", id);

		try {
			return new ResponseEntity<Quizz>(quizzService.getQuizzById(id), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * POST api/quizz - quizz creation
	 * 
	 * @param quizz the quizz
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity postQuizz(@RequestBody Quizz quizz) {
		LOGGER.info("POST api/quizz");

		try {
			return new ResponseEntity<Quizz>(quizzService.createQuizz(quizz), HttpStatus.CREATED);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * PUT api/quizz/{id} - quizz update
	 * 
	 * @param id    the quizz id
	 * @param quizz the quizz
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	public ResponseEntity putQuizz(@PathVariable(value = "id") Integer id, @RequestBody Quizz quizz) {
		LOGGER.info("PUT api/quizz/{}", id);

		try {
			return new ResponseEntity<Quizz>(quizzService.updateQuizz(id, quizz), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * DELETE api/quizz/{id} - quizz deletion
	 * 
	 * @param id the quizz id
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
	public ResponseEntity deleteQuizz(@PathVariable(value = "id") Integer id) {
		LOGGER.info("DELETE api/quizz/{}", id);

		try {
			quizzService.deleteQuizz(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * GET api/quizz/storagefile - quizzes storage file content
	 * 
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/storagefile", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public ResponseEntity getQuizzesFileContent() {
		LOGGER.info("GET api/quizz/storagefile");

		try {
			return new ResponseEntity<String>(quizzService.getQuizzesFileContent(), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * POST api/quizz/storagefile - quizzes storage file update
	 * 
	 * @param quizzesFileContent the quizzes storage file content
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/storagefile", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.POST)
	public ResponseEntity postQuizzesFileContent(@RequestBody String quizzesFileContent) {
		LOGGER.info("POST api/quizz/storagefile");

		try {
			quizzService.updateQuizzesFileContent(quizzesFileContent);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}
}
