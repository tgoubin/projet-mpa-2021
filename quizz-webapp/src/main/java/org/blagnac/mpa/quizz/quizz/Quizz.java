package org.blagnac.mpa.quizz.quizz;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

import org.blagnac.mpa.quizz.common.AbstractEntity;
import org.blagnac.mpa.quizz.question.Question;

/**
 * Description class for Quizz object
 * 
 * @author Thibault GOUBIN
 */
public class Quizz extends AbstractEntity {

	/**
	 * Questions list
	 */
	private List<Question> questions;

	/**
	 * String display
	 */
	private String stringDisplay;

	/**
	 * Constructor
	 */
	public Quizz() {
		questions = new ArrayList<>();
		stringDisplay = "";
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public String getStringDisplay() {
		stringDisplay = toString();
		return stringDisplay;
	}

	@Override
	public String toString() {
		OptionalDouble optionalDifficulty = questions.stream().mapToDouble(Question::getDifficultyNotation).average();
		String difficulty = (optionalDifficulty.isPresent())
				? new DecimalFormat("##0.00").format(optionalDifficulty.getAsDouble())
				: "?";
		return "#" + getId() + " - " + questions.size() + " question(s) - Difficulty : " + difficulty + " / 3";
	}
}
