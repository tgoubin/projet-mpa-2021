package org.blagnac.mpa.quizz.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Not Found exceptions
 * 
 * @author Thibault GOUBIN
 */
public class NotFoundException extends AbstractException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 5674388155425053328L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public NotFoundException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		// HTTP code for Not Found errors is 404
		return HttpStatus.NOT_FOUND.value();
	}
}
