package org.blagnac.mpa.quizz.player;

import java.util.List;

import org.blagnac.mpa.quizz.common.ApiConstants;
import org.blagnac.mpa.quizz.exception.AbstractException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * REST controller for Player
 * 
 * @author Thibault GOUBIN
 */
@Controller
@RequestMapping(value = PlayerController.API_PATH)
public class PlayerController {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerController.class);

	/**
	 * API path
	 */
	public static final String API_PATH = ApiConstants.API_BASE_PATH + "/player";

	/**
	 * Service for player
	 */
	@Autowired
	private PlayerService playerService;

	/**
	 * GET api/player/results - get all results
	 * 
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "results", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity getPlayerResults() {
		LOGGER.info("GET api/player/results");

		try {
			return new ResponseEntity<List<PlayerPlayQuizz>>(playerService.getAllResults(), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * POST api/player/validatequizz - quizz answers validation
	 * 
	 * @param questionsAnswers list of couples question / answer
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "validatequizz", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity postValidatePlayerQuizz(@RequestBody PlayerQuestionsAnswers questionsAnswers) {
		LOGGER.info("POST api/player/validatequizz");

		try {
			return new ResponseEntity<Integer>(playerService.validatePlayerQuizz(questionsAnswers), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}
}
