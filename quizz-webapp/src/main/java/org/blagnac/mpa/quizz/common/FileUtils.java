package org.blagnac.mpa.quizz.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Utilities for files
 * 
 * @author Thibault GOUBIN
 */
public class FileUtils {

	/**
	 * Returns a file content
	 * 
	 * @param filePath the file path
	 * @return the file content
	 * @throws IOException an IOException
	 */
	public static String getFileContent(String filePath) throws IOException {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8));
		StringBuilder fileContent = new StringBuilder();
		reader.lines().forEach(l -> {
			fileContent.append(l);
		});
		reader.close();
		return fileContent.toString();
	}

	/**
	 * Writes a content in a file
	 * 
	 * @param filePath    the file path
	 * @param fileContent the file content
	 * @param resetFile   if the file must be rewritten from scratch
	 * @throws IOException an IO exception
	 */
	public static void writeInFile(String filePath, String fileContent, boolean resetFile) throws IOException {
		if (resetFile) {
			File file = new File(filePath);
			file.delete();
			file.createNewFile();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
		writer.write(fileContent);
		writer.close();
	}
}
