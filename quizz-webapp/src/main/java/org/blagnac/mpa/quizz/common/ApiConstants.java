package org.blagnac.mpa.quizz.common;

/**
 * Constants for the REST API
 * 
 * @author Thibault GOUBIN
 */
public class ApiConstants {

	/**
	 * API base path
	 */
	public static final String API_BASE_PATH = "/api";
}
