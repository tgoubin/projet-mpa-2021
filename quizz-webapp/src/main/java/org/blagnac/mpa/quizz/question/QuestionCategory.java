package org.blagnac.mpa.quizz.question;

/**
 * Categories for a question
 * 
 * @author Thibault GOUBIN
 */
public enum QuestionCategory {

	SPORT, MUSIC, GEOGRAPHY, HISTORY, LITERATURE, SCIENCES;
}
