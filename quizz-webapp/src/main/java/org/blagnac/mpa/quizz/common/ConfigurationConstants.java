package org.blagnac.mpa.quizz.common;

/**
 * Constants for configuration variables
 * 
 * @author Thibault GOUBIN
 */
public class ConfigurationConstants {

	/**
	 * For configuration variable "server.servlet.context-path"
	 */
	public static final String SERVER_CONTEXT_PATH = "${server.servlet.context-path}";

	/**
	 * For configuration variable "server.port"
	 */
	public static final String SERVER_PORT = "${server.port}";
}
