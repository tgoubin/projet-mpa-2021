package org.blagnac.mpa.quizz.question;

import java.util.ArrayList;
import java.util.List;

import org.blagnac.mpa.quizz.common.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Description class for Question object
 * 
 * @author Thibault GOUBIN
 */
public class Question extends AbstractEntity {

	/**
	 * Text for the question
	 */
	private String text;

	/**
	 * The question category
	 */
	private QuestionCategory category;

	/**
	 * The question difficulty
	 */
	private QuestionDifficulty difficulty;

	/**
	 * Possible answers for the question
	 */
	private List<Answer> answers;

	/**
	 * Constructor
	 */
	public Question() {
		answers = new ArrayList<>();
	}

	/**
	 * Constructor
	 * 
	 * @param text       the text
	 * @param category   the category
	 * @param difficulty the difficulty
	 */
	public Question(String text, QuestionCategory category, QuestionDifficulty difficulty) {
		this.text = text;
		this.category = category;
		this.difficulty = difficulty;
		answers = new ArrayList<>();
	}

	/**
	 * Constructor
	 * 
	 * @param text       the text
	 * @param category   the category
	 * @param difficulty the difficulty
	 * @param answers    the answers
	 */
	public Question(String text, QuestionCategory category, QuestionDifficulty difficulty, List<Answer> answers) {
		this.text = text;
		this.category = category;
		this.difficulty = difficulty;
		this.answers = answers;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public QuestionCategory getCategory() {
		return category;
	}

	public void setCategory(QuestionCategory category) {
		this.category = category;
	}

	public QuestionDifficulty getDifficulty() {
		return difficulty;
	}

	@JsonIgnore
	public int getDifficultyNotation() {
		return (difficulty != null) ? difficulty.getNotation() : 0;
	}

	public void setDifficulty(QuestionDifficulty difficulty) {
		this.difficulty = difficulty;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
}
