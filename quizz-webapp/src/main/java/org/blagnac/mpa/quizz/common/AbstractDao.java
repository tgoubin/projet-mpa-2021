package org.blagnac.mpa.quizz.common;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object abstract class
 * 
 * @author Thibault GOUBIN
 */
@Component
public abstract class AbstractDao<T extends AbstractEntity> {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);

	/**
	 * Returns the data file content corresponding to the generic data class
	 * 
	 * @param typeReference the type reference to return
	 * @return the data file content
	 * @throws IOException an IO exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> findAll(TypeReference typeReference) throws IOException {
		return (List<T>) DataStorageConstants.JSON_MAPPER.readValue(getStorageFileContent(), typeReference);
	}

	/**
	 * Returns the raw data file content
	 * 
	 * @return the raw data file content
	 * @throws IOException an IO exception
	 */
	public String getStorageFileContent() throws IOException {
		return FileUtils.getFileContent(dataFilePath());
	}

	/**
	 * Updates the raw data file content
	 * 
	 * @param storageFileContent the storageFileContent
	 * @throws IOException an IO exception
	 */
	public void updateStorageFileContent(String storageFileContent) throws IOException {
		FileUtils.writeInFile(dataFilePath(), storageFileContent, true);
	}

	/**
	 * Finds all the data corresponding to the generic data class
	 * 
	 * @return the data list
	 * @throws IOException an IO exception
	 */
	public abstract List<T> findAll() throws IOException;

	/**
	 * Finds the data item (corresponding to the generic data class) with a given id
	 * 
	 * @param id the data id
	 * @return the data list
	 * @throws IOException an IO exception
	 */
	public Optional<T> findById(Integer id) throws IOException {
		return findAll().stream().filter(d -> d.getId() == id).findFirst();
	}

	/**
	 * Creates a data item
	 * 
	 * @param objectToCreate object to create
	 * @return the object created
	 * @throws IOException an IO exception
	 */
	public T create(T objectToCreate) throws IOException {
		List<T> allObjects = findAll();

		objectToCreate.setId(allObjects.size() + 1);
		allObjects.add(objectToCreate);

		FileUtils.writeInFile(dataFilePath(), DataStorageConstants.JSON_MAPPER.writeValueAsString(allObjects), true);

		LOGGER.info("'{}' (id = '{}') object created in '{}' file", objectToCreate.getClass().getSimpleName(),
				objectToCreate.getId(), dataFilePath());

		return objectToCreate;
	}

	/**
	 * Updates a data item
	 * 
	 * @param id             the data item id
	 * @param objectToUpdate the object to update
	 * @return the object updated
	 * @throws IOException an IO exception
	 */
	public T update(Integer id, T objectToUpdate) throws IOException {
		List<T> allObjects = findAll();

		allObjects.removeIf(d -> d.getId() == id);
		allObjects.add(objectToUpdate);

		FileUtils.writeInFile(dataFilePath(), DataStorageConstants.JSON_MAPPER.writeValueAsString(allObjects), true);

		LOGGER.info("'{}' (id = '{}') object updated in '{}' file", objectToUpdate.getClass().getSimpleName(), id,
				dataFilePath());

		return objectToUpdate;
	}

	/**
	 * Deletes a data item
	 * 
	 * @param id the data item id
	 * @throws IOException an IO exception
	 */
	public void delete(Integer id) throws IOException {
		List<T> allObjects = findAll();
		String objectClassName = allObjects.get(0).getClass().getSimpleName();

		allObjects.removeIf(d -> d.getId() == id);

		FileUtils.writeInFile(dataFilePath(), DataStorageConstants.JSON_MAPPER.writeValueAsString(allObjects), true);

		LOGGER.info("'{}' (id = '{}') object deleted in '{}' file", objectClassName, id, dataFilePath());
	}

	/**
	 * Returns the data file name corresponding to the generic data class
	 * 
	 * @return the data file name
	 */
	public abstract String dataFileName();

	/**
	 * Returns the data file path corresponding to the generic data class
	 * 
	 * @return the data file path
	 */
	public String dataFilePath() {
		return DataStorageConstants.DATA_STORAGE_PATH + dataFileName();
	}
}
