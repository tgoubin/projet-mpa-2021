package org.blagnac.mpa.quizz.question;

/**
 * Description class for Answer object
 * 
 * @author Thibault GOUBIN
 */
public class Answer {

	/**
	 * Text for the answer
	 */
	private String text;

	/**
	 * If the answer is correct or not
	 */
	private Boolean correct;

	/**
	 * Constructor
	 */
	public Answer() {
		correct = false;
	}

	/**
	 * Constructor
	 * 
	 * @param text    the text
	 * @param correct if the answer is correct or not
	 */
	public Answer(String text, boolean correct) {
		this.text = text;
		this.correct = correct;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean isCorrect() {
		return correct;
	}

	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}
}
