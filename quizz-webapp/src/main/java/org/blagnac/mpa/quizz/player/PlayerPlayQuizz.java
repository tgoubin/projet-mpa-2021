package org.blagnac.mpa.quizz.player;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Description class for Player plays Quizz object
 * 
 * @author Thibault GOUBIN
 */
public class PlayerPlayQuizz {

	/**
	 * The Quizz id
	 */
	private Integer quizzId;

	/**
	 * The player name
	 */
	private String playerName;

	/**
	 * The score
	 */
	private int score;

	/**
	 * The number of questions
	 */
	private int nbQuestions;

	/**
	 * Date / time of the quizz
	 */
	private long dateTime;

	/**
	 * Constructor
	 */
	public PlayerPlayQuizz() {
		score = 0;
		nbQuestions = 0;
		dateTime = new Date().getTime();
	}

	/**
	 * Constructor
	 * 
	 * @param quizzId     the quizz id
	 * @param playerName  the player name
	 * @param score       the score
	 * @param nbQuestions the number of questions
	 * @param dateTime    the date / time
	 */
	public PlayerPlayQuizz(Integer quizzId, String playerName, int score, int nbQuestions, long dateTime) {
		this.quizzId = quizzId;
		this.playerName = playerName;
		this.score = score;
		this.nbQuestions = nbQuestions;
		this.dateTime = dateTime;
	}

	public Integer getQuizzId() {
		return quizzId;
	}

	public void setQuizzId(Integer quizzId) {
		this.quizzId = quizzId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getNbQuestions() {
		return nbQuestions;
	}

	public void setNbQuestions(int nbQuestions) {
		this.nbQuestions = nbQuestions;
	}

	@JsonIgnore
	public float getPercentage() {
		return ((float) score / (float) nbQuestions) * 100;
	}

	public long getDateTime() {
		return dateTime;
	}

	public void setDateTime(long dateTime) {
		this.dateTime = dateTime;
	}
}
