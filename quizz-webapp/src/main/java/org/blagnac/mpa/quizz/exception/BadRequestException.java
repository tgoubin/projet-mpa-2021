package org.blagnac.mpa.quizz.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Bad Request exceptions
 * 
 * @author Thibault GOUBIN
 */
public class BadRequestException extends AbstractException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -3577265097153524191L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public BadRequestException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		return HttpStatus.BAD_REQUEST.value();
	}
}
