package org.blagnac.mpa.quizz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main class for Quizz REST SpringBoot Application
 * 
 * @author Thibault GOUBIN
 */
@SpringBootApplication(scanBasePackages = "org.blagnac.mpa.quizz")
@ComponentScan({ "org.blagnac.mpa.quizz" })
public class QuizzApplication extends SpringBootServletInitializer {

	/**
	 * Main method
	 * 
	 * @param args the program arguments (no argument in this case)
	 */
	public static void main(String[] args) {
		SpringApplication.run(QuizzApplication.class, args);
	}
}
