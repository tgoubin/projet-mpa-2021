package org.blagnac.mpa.quizz.question;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.blagnac.mpa.quizz.common.ApiConstants;
import org.blagnac.mpa.quizz.exception.AbstractException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * REST controller for Question
 * 
 * @author Thibault GOUBIN
 */
@Controller
@RequestMapping(value = QuestionController.API_PATH)
public class QuestionController {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuestionController.class);

	/**
	 * API path
	 */
	public static final String API_PATH = ApiConstants.API_BASE_PATH + "/question";

	/**
	 * Service for question objects
	 */
	@Autowired
	private QuestionService questionService;

	/**
	 * GET api/question - questions listing
	 * 
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity getQuestions() {
		LOGGER.info("GET api/question");

		try {
			return new ResponseEntity<List<Question>>(questionService.getAllQuestions(), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * GET api/question/{id} - question by id
	 * 
	 * @param id       the question id
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity getQuestionById(@PathVariable(value = "id") Integer id) {
		LOGGER.info("GET api/question/{}", id);

		try {
			return new ResponseEntity<Question>(questionService.getQuestionById(id), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * POST api/question - question creation
	 * 
	 * @param question the question
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity postQuestion(@RequestBody Question question) {
		LOGGER.info("POST api/question");

		try {
			return new ResponseEntity<Question>(questionService.createQuestion(question), HttpStatus.CREATED);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * PUT api/question/{id} - question update
	 * 
	 * @param id       the question id
	 * @param question the question
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	public ResponseEntity putQuestion(@PathVariable(value = "id") Integer id, @RequestBody Question question) {
		LOGGER.info("PUT api/question/{}", id);

		try {
			return new ResponseEntity<Question>(questionService.updateQuestion(id, question), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * DELETE api/question/{id} - question deletion
	 * 
	 * @param id the question id
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
	public ResponseEntity deleteQuestion(@PathVariable(value = "id") Integer id) {
		LOGGER.info("DELETE api/question/{}", id);

		try {
			questionService.deleteQuestion(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * GET api/question/storagefile - questions storage file content
	 * 
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/storagefile", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public ResponseEntity getQuestionsFileContent() {
		LOGGER.info("GET api/question/storagefile");

		try {
			return new ResponseEntity<String>(questionService.getQuestionsFileContent(), HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}

	/**
	 * GET api/question/category - question categories
	 * 
	 * @return the HTTP response
	 */
	@RequestMapping(value = "/category", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<List<String>> getQuestionCategories() {
		LOGGER.info("GET api/question/category");
		return new ResponseEntity<List<String>>(
				Arrays.asList(QuestionCategory.values()).stream().map(c -> c.name()).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	/**
	 * GET api/question/difficulty - question difficulties
	 * 
	 * @return the HTTP response
	 */
	@RequestMapping(value = "/difficulty", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<List<String>> getQuestionDifficulties() {
		LOGGER.info("GET api/question/difficulty");
		return new ResponseEntity<List<String>>(
				Arrays.asList(QuestionDifficulty.values()).stream().map(c -> c.name()).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	/**
	 * POST api/question/storagefile - questions storage file update
	 * 
	 * @param questionsFileContent the questions storage file content
	 * @return the HTTP response
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/storagefile", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.POST)
	public ResponseEntity postQuizzesFileContent(@RequestBody String questionsFileContent) {
		LOGGER.info("POST api/question/storagefile");

		try {
			questionService.updateQuestionsFileContent(questionsFileContent);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (AbstractException e) {
			return new ResponseEntity<String>(e.getErrorMessage(), HttpStatus.valueOf(e.getHttpStatusCode()));
		}
	}
}
