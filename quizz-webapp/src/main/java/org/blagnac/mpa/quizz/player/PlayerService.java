package org.blagnac.mpa.quizz.player;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.blagnac.mpa.quizz.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.exception.TechnicalException;
import org.blagnac.mpa.quizz.question.QuestionService;
import org.blagnac.mpa.quizz.quizz.Quizz;
import org.blagnac.mpa.quizz.quizz.QuizzService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for Player
 * 
 * @author Thibault GOUBIN
 */
@Service
public class PlayerService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerService.class);

	/**
	 * Service for quizz objects
	 */
	@Autowired
	private QuizzService quizzService;

	/**
	 * Service for question objects
	 */
	@Autowired
	private QuestionService questionService;

	/**
	 * DAO for PlayerPlayQuizz objects
	 */
	@Autowired
	private PlayerPlayQuizzDao playerPlayQuizzDao;

	/**
	 * Finds all results
	 * 
	 * @return the results list
	 */
	public List<PlayerPlayQuizz> getAllResults() {
		LOGGER.info("Results listing");

		try {
			return playerPlayQuizzDao.findAll();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Quizz answers validation and result saving
	 * 
	 * @param questionsAnswers list of couples question / answer
	 * @return the final score
	 */
	public int validatePlayerQuizz(PlayerQuestionsAnswers questionsAnswers) {
		LOGGER.info("Player '{}' quizz (id = '{}') validation", questionsAnswers.getPlayerName(),
				questionsAnswers.getQuizzId());

		try {
			Quizz quizz = quizzService.getQuizzById(questionsAnswers.getQuizzId());

			ExceptionUtils.throwBadRequestExceptionIf(
					questionsAnswers.getPlayerName() == null || "".equals(questionsAnswers.getPlayerName().trim()),
					"The player name is mandatory", LOGGER);

			int score = 0;
			for (Map.Entry<Integer, Integer> questionAnswer : questionsAnswers.getQuestionsAnswers().entrySet()) {
				score += (questionService.getQuestionById(questionAnswer.getKey()).getAnswers()
						.get(questionAnswer.getValue()).isCorrect()) ? 1 : 0;
			}

			playerPlayQuizzDao.add(new PlayerPlayQuizz(quizz.getId(), questionsAnswers.getPlayerName().trim(), score,
					questionsAnswers.getQuestionsAnswers().size(), new Date().getTime()));

			return score;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}
}
