package org.blagnac.mpa.quizz.question;

import java.io.IOException;
import java.util.List;

import org.blagnac.mpa.quizz.common.AbstractDao;
import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object for Question
 * 
 * @author Thibault GOUBIN
 */
@Component
public class QuestionDao extends AbstractDao<Question> {

	@Override
	public List<Question> findAll() throws IOException {
		return findAll(new TypeReference<List<Question>>() {
		});
	}

	@Override
	public String dataFileName() {
		return DataStorageConstants.QUESTIONS_FILE;
	}
}
