package org.blagnac.mpa.quizz.exception;

import org.slf4j.Logger;

/**
 * Utilities for exception
 * 
 * @author Thibault GOUBIN
 */
public class ExceptionUtils {

	/**
	 * Throws a BadRequestException if a condition is checked
	 * 
	 * @param badRequestCondition the BadRequest condition
	 * @param errorMessage        the error message
	 * @param logger              the logger
	 */
	public static void throwBadRequestExceptionIf(boolean badRequestCondition, String errorMessage, Logger logger) {
		if (badRequestCondition) {
			logger.warn(errorMessage);
			throw new BadRequestException(errorMessage);
		}
	}

	/**
	 * Throws a NotFoundException if a condition is checked
	 * 
	 * @param notFoundCondition the NotFound condition
	 * @param errorMessage      the error message
	 * @param logger            the logger
	 */
	public static void throwNotFoundExceptionIf(boolean notFoundCondition, String errorMessage, Logger logger) {
		if (notFoundCondition) {
			logger.warn(errorMessage);
			throw new NotFoundException(errorMessage);
		}
	}
}
