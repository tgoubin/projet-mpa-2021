package org.blagnac.mpa.quizz.quizz;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.exception.TechnicalException;
import org.blagnac.mpa.quizz.question.Question;
import org.blagnac.mpa.quizz.question.QuestionDao;
import org.blagnac.mpa.quizz.question.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Service for Quizz
 * 
 * @author Thibault GOUBIN
 */
@Service
public class QuizzService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuizzService.class);

	/**
	 * Service for question objects
	 */
	@Autowired
	private QuestionService questionService;

	/**
	 * DAO for quizz objects
	 */
	@Autowired
	private QuizzDao quizzDao;

	/**
	 * DAO for question objects
	 */
	@Autowired
	private QuestionDao questionDao;

	/**
	 * Finds all quizzes
	 * 
	 * @return the quizzes list
	 */
	public List<Quizz> getAllQuizzes() {
		LOGGER.info("Quizzes listing");

		try {
			List<Quizz> quizzes = quizzDao.findAll();

			for (Quizz quizz : quizzes) {
				enrichQuizz(quizz, true);
			}

			return quizzes;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Returns a quizz by a given id
	 * 
	 * @param id the quizz id
	 * @return the quizz
	 */
	public Quizz getQuizzById(Integer id) {
		try {
			Optional<Quizz> optionalQuizz = quizzDao.findById(id);

			ExceptionUtils.throwNotFoundExceptionIf(!optionalQuizz.isPresent(),
					"Quizz with id '" + id.toString() + "' does not exist", LOGGER);

			Quizz quizz = optionalQuizz.get();
			enrichQuizz(optionalQuizz.get(), true);
			return quizz;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Creates a quizz
	 * 
	 * @param quizz the quizz
	 * @return the quizz created
	 */
	public Quizz createQuizz(Quizz quizz) {
		LOGGER.info("Quizz creation - {} questions", quizz.getQuestions().size());

		try {
			createOrUpdateCheckQuizzFields(quizz);

			Quizz quizzCreated = quizzDao.create(quizz);
			enrichQuizz(quizzCreated, false);
			return quizzCreated;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Updates a quizz
	 * 
	 * @param id    the quizz id
	 * @param quizz the quizz
	 * @return the quizz updated
	 */
	public Quizz updateQuizz(Integer id, Quizz quizz) {
		LOGGER.info("Quizz deletion : id = '{}'", id);

		try {
			createOrUpdateCheckQuizzFields(quizz);

			Quizz quizzToUpdate = getQuizzById(id);

			// Fields updating
			quizzToUpdate.setQuestions(quizz.getQuestions());

			Quizz quizzUpdated = quizzDao.update(id, quizz);
			enrichQuizz(quizzUpdated, false);
			return quizzUpdated;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Enriches a quizz object (with questions details)
	 * 
	 * @param quizz              the quizz
	 * @param hideCorrectAnswers if the correct answers must be hidden or not
	 * @throws IOException an IO exception
	 */
	private void enrichQuizz(Quizz quizz, boolean hideCorrectAnswers) throws IOException {
		if (quizz.getQuestions() != null && !quizz.getQuestions().isEmpty()) {
			for (Question questionInQuizz : quizz.getQuestions()) {
				questionService.enrichQuestion(questionInQuizz, hideCorrectAnswers);
			}
		}
	}

	/**
	 * Checks for quizz creation or update
	 * 
	 * @param quizz the quizz
	 * @throws IOException an IO exception
	 */
	private void createOrUpdateCheckQuizzFields(Quizz quizz) throws IOException {
		// Mandatory fields
		ExceptionUtils.throwBadRequestExceptionIf(quizz.getQuestions() == null || quizz.getQuestions().isEmpty(),
				"The quizz must contain at least 1 question", LOGGER);

		// Questions existence checking
		checkAssociatedQuestions(quizz);
	}

	/**
	 * Checks quizz associated questions
	 * 
	 * @param quizz the quizz
	 * @throws IOException an IO exception
	 */
	private void checkAssociatedQuestions(Quizz quizz) throws IOException {
		for (Question question : quizz.getQuestions()) {
			ExceptionUtils.throwBadRequestExceptionIf(!questionDao.findById(question.getId()).isPresent(),
					"The question with id '" + question.getId() + "' does not exit", LOGGER);
		}
	}

	/**
	 * Deletes a quizz
	 * 
	 * @param id the quizz id
	 */
	public void deleteQuizz(Integer id) {
		LOGGER.info("Quizz deletion : id = '{}'", id);

		try {
			// Just for id check
			getQuizzById(id);

			quizzDao.delete(id);
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Returns the quizzes data file content
	 * 
	 * @return the quizzes data file content
	 */
	public String getQuizzesFileContent() {
		try {
			return quizzDao.getStorageFileContent();
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Updates the questions data file content
	 * 
	 * @param quizzesFileContent the quizzes file content
	 */
	public void updateQuizzesFileContent(String quizzesFileContent) {
		try {
			// We try to map in object in order to validate the JSON string
			List<Quizz> quizzes = (List<Quizz>) DataStorageConstants.JSON_MAPPER.readValue(quizzesFileContent,
					new TypeReference<List<Quizz>>() {
					});

			// Associated questions check
			for (Quizz quizz : quizzes) {
				checkAssociatedQuestions(quizz);
			}

			// If OK
			quizzDao.updateStorageFileContent(quizzesFileContent);
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}
}
