package org.blagnac.mpa.quizz.exception;

import org.springframework.http.HttpStatus;

/**
 * Class for Technical exceptions
 * 
 * @author Thibault GOUBIN
 */
public class TechnicalException extends AbstractException {

	/**
	 * Serial UUID
	 */
	private static final long serialVersionUID = -8363038184188897198L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public TechnicalException(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public int getHttpStatusCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR.value();
	}
}
