package org.blagnac.mpa.quizz.question;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.exception.TechnicalException;
import org.blagnac.mpa.quizz.quizz.Quizz;
import org.blagnac.mpa.quizz.quizz.QuizzDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Service for Question
 * 
 * @author Thibault GOUBIN
 */
@Service
public class QuestionService {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuestionService.class);

	/**
	 * DAO for question objects
	 */
	@Autowired
	private QuestionDao questionDao;

	/**
	 * DAO for quizz objects
	 */
	@Autowired
	private QuizzDao quizzDao;

	/**
	 * Finds all questions
	 * 
	 * @return the questions list
	 */
	public List<Question> getAllQuestions() {
		LOGGER.info("Questions listing");

		try {
			List<Question> questions = questionDao.findAll();

			for (Question question : questions) {
				enrichQuestion(question, false);
			}

			return questions;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Returns a question by a given id
	 * 
	 * @param id the question id
	 * @return the question
	 */
	public Question getQuestionById(Integer id) {
		try {
			Optional<Question> optionalQuestion = questionDao.findById(id);

			ExceptionUtils.throwNotFoundExceptionIf(!optionalQuestion.isPresent(),
					"Question with id '" + id.toString() + "' does not exist", LOGGER);

			return optionalQuestion.get();
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Creates a question
	 * 
	 * @param question the question
	 * @return the question created
	 */
	public Question createQuestion(Question question) {
		LOGGER.info("Question creation : '{}'", question.getText());

		try {
			createOrUpdateCheckQuestionFields(question);

			// Fields cleaning
			question.setText(question.getText().trim());
			for (Answer answer : question.getAnswers()) {
				answer.setText(answer.getText().trim());
			}

			Question questionCreated = questionDao.create(question);
			enrichQuestion(questionCreated, false);
			return questionCreated;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Updates a question
	 * 
	 * @param id       the question id
	 * @param question the question
	 * @return the question updated
	 */
	public Question updateQuestion(Integer id, Question question) {
		LOGGER.info("Question update : id = '{}'", id);

		try {
			createOrUpdateCheckQuestionFields(question);

			Question questionToUpdate = getQuestionById(id);

			// Fields updating and cleaning
			questionToUpdate.setCategory(question.getCategory());
			questionToUpdate.setDifficulty(question.getDifficulty());
			questionToUpdate.setText(question.getText().trim());
			for (Answer answer : question.getAnswers()) {
				answer.setText(answer.getText().trim());
			}
			questionToUpdate.setAnswers(question.getAnswers());

			Question questionUpdated = questionDao.update(id, questionToUpdate);
			enrichQuestion(questionUpdated, false);
			return questionUpdated;
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Enriches a question object
	 * 
	 * @param question           the question
	 * @param hideCorrectAnswers if the correct answers must be hidden or not
	 * @throws IOException an IO exception
	 */
	public void enrichQuestion(Question question, boolean hideCorrectAnswers) throws IOException {
		Optional<Question> optionalQuestion = questionDao.findById(question.getId());

		if (optionalQuestion.isPresent()) {
			question.setAnswers(optionalQuestion.get().getAnswers());

			if (hideCorrectAnswers) {
				for (Answer answer : question.getAnswers()) {
					answer.setCorrect(null);
				}
			}

			question.setCategory(optionalQuestion.get().getCategory());
			question.setDifficulty(optionalQuestion.get().getDifficulty());
			question.setText(optionalQuestion.get().getText());
		}
	}

	/**
	 * Question fields checks
	 * 
	 * @param question the question
	 */
	private void createOrUpdateCheckQuestionFields(Question question) {
		// Mandatory fields
		ExceptionUtils.throwBadRequestExceptionIf(question.getCategory() == null, "The question category is mandatory",
				LOGGER);
		ExceptionUtils.throwBadRequestExceptionIf(question.getDifficulty() == null,
				"The question difficulty is mandatory", LOGGER);
		ExceptionUtils.throwBadRequestExceptionIf(question.getText() == null || "".equals(question.getText().trim()),
				"The question text is mandatory", LOGGER);

		// Answers checking
		ExceptionUtils.throwBadRequestExceptionIf(question.getAnswers() == null || question.getAnswers().size() < 2,
				"A question must have minimum 2 possible answers", LOGGER);
		ExceptionUtils.throwBadRequestExceptionIf(
				question.getAnswers().stream().filter(a -> a.isCorrect()).count() != 1,
				"A question must have 1 correct answer", LOGGER);
		ExceptionUtils.throwBadRequestExceptionIf(question.getAnswers().stream()
				.filter(a -> a.getText() == null || "".equals(a.getText().trim())).count() > 0,
				"The answer text is mandatory", LOGGER);
	}

	/**
	 * Deletes a question
	 * 
	 * @param id the question id
	 */
	public void deleteQuestion(Integer id) {
		LOGGER.info("Question deletion : id = '{}'", id);

		try {
			// Just for id check
			getQuestionById(id);

			for (Quizz quizz : quizzDao.findContainingQuestion(id)) {
				quizz.getQuestions().removeIf(q -> q.getId() == id);
				quizzDao.update(quizz.getId(), quizz);
			}

			questionDao.delete(id);
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Returns the questions data file content
	 * 
	 * @return the questions data file content
	 */
	public String getQuestionsFileContent() {
		try {
			return questionDao.getStorageFileContent();
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}

	/**
	 * Updates the questions data file content
	 * 
	 * @param questionsFileContent the questions file content
	 */
	public void updateQuestionsFileContent(String questionsFileContent) {
		try {
			// We try to map in object in order to validate the JSON string
			DataStorageConstants.JSON_MAPPER.readValue(questionsFileContent, new TypeReference<List<Question>>() {
			});

			// If OK
			questionDao.updateStorageFileContent(questionsFileContent);
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new TechnicalException(e.getMessage());
		}
	}
}
