package org.blagnac.mpa.quizz.player;

import java.io.IOException;
import java.util.List;

import org.blagnac.mpa.quizz.common.DataStorageConstants;
import org.blagnac.mpa.quizz.common.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object for PlayerPlayQuizz
 * 
 * @author Thibault GOUBIN
 */
@Component
public class PlayerPlayQuizzDao {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerPlayQuizzDao.class);

	/**
	 * Returns all the PlayerPlayQuizz objects saved in data file
	 * 
	 * @return the PlayerPlayQuizz objects
	 * @throws IOException an IOException
	 */
	public List<PlayerPlayQuizz> findAll() throws IOException {
		return (List<PlayerPlayQuizz>) DataStorageConstants.JSON_MAPPER.readValue(
				FileUtils.getFileContent(getResultsDataFilePath()), new TypeReference<List<PlayerPlayQuizz>>() {
				});
	}

	/**
	 * Adds a PlayerPlayQuizz
	 * 
	 * @param playerPlayQuizz playerPlayQuizz to add
	 * @throws IOException an IO exception
	 */
	public void add(PlayerPlayQuizz playerPlayQuizz) throws IOException {
		List<PlayerPlayQuizz> allResults = findAll();

		allResults.add(playerPlayQuizz);

		FileUtils.writeInFile(getResultsDataFilePath(), DataStorageConstants.JSON_MAPPER.writeValueAsString(allResults),
				true);

		LOGGER.info("'{}' ({} / {}) object created in '{}' file", playerPlayQuizz.getClass().getSimpleName(),
				playerPlayQuizz.getPlayerName(), playerPlayQuizz.getDateTime(), getResultsDataFilePath());
	}

	/**
	 * Results data file path
	 * 
	 * @return the results data file path
	 */
	private String getResultsDataFilePath() {
		return DataStorageConstants.DATA_STORAGE_PATH + DataStorageConstants.RESULTS_FILE;
	}
}
